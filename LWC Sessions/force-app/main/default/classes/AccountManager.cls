public with sharing class AccountManager {
    
    /*@AuraEnabled(cacheable=true)
    public static List<Account> getAccounts (){
        try {
            return [select Id,name,phone,website from account limit 10];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }*/
    @AuraEnabled
    public static List<Account> getAccounts (Integer noOfRecords){
        try {
            return [select Id,name,phone,website from account limit :noOfRecords];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}
