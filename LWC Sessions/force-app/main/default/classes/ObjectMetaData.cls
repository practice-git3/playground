public with sharing class ObjectMetaData {
    
    @AuraEnabled(cacheable=true)
    public static map<string, string>  getAllObjects(){

        map<string, string> objectList = new map<string, string>();
        try {

            for ( Schema.SObjectType o : Schema.getGlobalDescribe().values() )
            {
                Schema.DescribeSObjectResult objResult = o.getDescribe();
                if(objResult.isAccessible()){

                    objectList.put(objResult.getName(), objResult.getLabel());
                }  
            }           
        } 
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return objectList;
    }


    @AuraEnabled(cacheable=true)
    public static string objectFieldsData(String objectName){

        System.debug('objectName' +objectName);

        List<FieldWrapper> fieldWrapperList = new List<FieldWrapper>();
        try {
            
            Map<String,Schema.SObjectField> fieldMap  = Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap();

            for(Schema.SObjectField sfield :fieldMap.Values()){

                Schema.DescribeFieldResult dfield = sfield.getDescribe();
                if(dfield.isAccessible()){

                    FieldWrapper fWrap = new FieldWrapper();
                    fWrap.fieldName = dfield.getName();
                    fWrap.fieldLabel = dfield.getLabel();
                    fWrap.fieldDataType = dfield.getType();

                    //System.debug('fWrap '+fWrap);
                    fieldWrapperList.add(fWrap);
                }
            }

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

        System.debug('fieldWrapperList '+fieldWrapperList);
        return JSON.serialize(fieldWrapperList);
       
    }

    public class FieldWrapper{

        public String fieldName;
        public string fieldLabel;
        public Schema.DisplayType fieldDataType; 
    }

    
}
