({
    doInit : function(component, event, helper) {

        component.set("v.meetingRoomInfo",[
            {roomName:'A-01',roomCapacity:1},
            {roomName:'A-02',roomCapacity:3},
            {roomName:'A-03',roomCapacity:5},
            {roomName:'B-01',roomCapacity:2},
            {roomName:'B-02',roomCapacity:1},
            {roomName:'C-01',roomCapacity:1},
        ]);

    },
    tileClickHandler : function(component, event, helper) {

        component.set("v.selectedRoomName",event.getParam("roomName"));

    }
})
