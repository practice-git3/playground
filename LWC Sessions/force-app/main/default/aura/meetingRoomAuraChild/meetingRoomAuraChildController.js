({
    tileClickHandler : function(component, event, helper) {

        var cmpEvent = component.getEvent("titleclick");
        var roomData = component.get("v.meetingRoomInfo");

        cmpEvent.setParams({

            "detail":roomData
            
        });

        cmpEvent.fire();


    }
})
