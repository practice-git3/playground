({
    sendHandler : function(component, event, helper) {

        const inputElement = component.find("text");
        if(inputElement){

            const msg = inputElement.get("v.value");
            const message = component.get("v.messages");

            message.push({
                id:message.length,
                value:msg,
                from:'AURA'
            });
            component.set("v.messages",message);
            const msgPayload={
                message:msg,
                from:'AURA'
            }
            const msgChannel = component.find("messagechannel");
            msgChannel.publish(msgPayload);
            inputElement.set("v.value","");
        }

    },
    messageHandler:function(component, event, helper) {

        if(event && event.getParam("message") && event.getParam("from") !== 'AURA'){

            const msg = event.getParam("message");
            const message = component.get("v.messages");

            message.push({
                id:message.length,
                value:msg,
                from:'LWC'
            });
            component.set("v.messages",message);
        }
        
    },
})
