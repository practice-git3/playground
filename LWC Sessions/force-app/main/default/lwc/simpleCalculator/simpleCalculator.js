 import { LightningElement } from 'lwc';

export default class SimpleCalculator extends LightningElement {

    currentResult;
    firstNumber;
    secondNumber;
    previousResult =[];
    showPrevResult = false;
    numberChangeHandler(event){

        const inputBoxName = event.target.name;

        if(inputBoxName === 'firstNumber'){
            this.firstNumber = event.target.value;
        }
        else if(inputBoxName === 'secondNumber'){
            this.secondNumber = event.target.value;
        }
    }

    addHandler(){

        const firstNum = parseInt(this.firstNumber);
        const secondNum = parseInt(this.secondNumber);

        //this.currentResult = 'Result of'+firstNum+' +'+secondNum+'is'+(firstNum+secondNum);

        this.currentResult = `Result of ${firstNum} + ${secondNum} is ${firstNum+secondNum}`;
        this.previousResult.push(this.currentResult);
    }

    subHandler(){

        const firstNum = parseInt(this.firstNumber);
        const secondNum = parseInt(this.secondNumber);

        //this.currentResult = 'Result of'+firstNum+' +'+secondNum+'is'+(firstNum+secondNum);

        this.currentResult = `Result of ${firstNum} - ${secondNum} is ${firstNum-secondNum}`;
        this.previousResult.push(this.currentResult);
    }

    mulHandler(){

        const firstNum = parseInt(this.firstNumber);
        const secondNum = parseInt(this.secondNumber);

        //this.currentResult = 'Result of'+firstNum+' +'+secondNum+'is'+(firstNum+secondNum);

        this.currentResult = `Result of ${firstNum} x ${secondNum} is ${firstNum*secondNum}`;
        this.previousResult.push(this.currentResult);
    }

    divHandler(){

        const firstNum = parseInt(this.firstNumber);
        const secondNum = parseInt(this.secondNumber);

       // this.currentResult = 'Result of'+firstNum+' +'+secondNum+'is'+(firstNum+secondNum);

        this.currentResult = `Result of ${firstNum} / ${secondNum} is ${firstNum/secondNum}`;
        this.previousResult.push(this.currentResult);
    }

    showPreviousResultToggle(event){
        this.showPrevResult = event.target.checked;
    }
}