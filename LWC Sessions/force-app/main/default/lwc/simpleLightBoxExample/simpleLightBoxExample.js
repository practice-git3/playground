import { LightningElement } from 'lwc';
import lb from '@salesforce/resourceUrl/simplelightbox';
import lb1 from '@salesforce/resourceUrl/simple';
// Example :- import TRAILHEAD_LOGO from '@salesforce/resourceUrl/trailhead_logo';';
import{loadScript, loadStyle} from 'lightning/platformResourceLoader';
// Example :- import TRAILHEAD_LOGO from '@salesforce/resourceUrl/trailhead_logo';

export default class SimpleLightBoxExample extends LightningElement {


    isscriptLoaded=false;

    renderedCallback(){

        if(!this.isscriptLoaded){
            debugger;
            Promise.all([
                loadStyle(this, lb1),
                loadScript(this, lb)
            ]).then(()=>{
                this.isscriptLoaded= true;
            }).catch((error) =>{
               
                console.error('Could not initilize simple light box-', error);
            });
        }
    }

    openGalleryHandler(){

        SimpleLightBox.open({
            items: ['/resource/cars/van/maruti_suzuki_eeco.jpg', '/resource/cars/luxury/mercedes_benz_gls.jpg', '/resource/cars/sports/Audi_R8_V10_Plus.jpg']
        
        });
    }
}