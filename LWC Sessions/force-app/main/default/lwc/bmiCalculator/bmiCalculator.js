import { LightningElement ,track} from 'lwc';
import{getBMI} from 'c/bmiCal';

export default class BmiCalculator extends LightningElement {

    cardTitle="BMI Calculator";
    //weight;
    //height;
    @track bmiData = {
        weight:0,
        height:0,
        result:0
    }

    weightChangeHandler(event){
        this.bmiData.weight = parseFloat(event.target.value);

    }

    heightChangeHandler(event){
        this.bmiData.height = parseFloat(event.target.value);
        
    }

    calculateBMI(){

        try{
            //this.bmiData.result = this.bmiData.weight/(this.bmiData.height*this.bmiData.height);
            this.bmiData.result = getBMI(this.bmiData.weight,this.bmiData.height);
        }
        catch(error){
            this.bmiData.result = undefined;
        }
        
    }

    get bmiValue(){

        return `Your BMI is : ${this.bmiData.result}`;
    }

}