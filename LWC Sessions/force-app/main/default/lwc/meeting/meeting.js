import { LightningElement,wire,track } from 'lwc';
import meetingInfo from'@salesforce/messageChannel/meetingInfo__c';
import{subscribe,unsubscribe,MessageContext,APPLICATION_SCOPE} from 'lightning/messageService';
export default class Meeting extends LightningElement {

    @wire(MessageContext)meetingContext;
    subscription = null;
    @track meetingData={};
    isMeetingDataFound = false;
    connectedCallback(){

        if(!this.subscription){
            this.subscription = subscribe(this.meetingContext,meetingInfo,(response) =>{
                this.meetingDetails(response);
            });
        }
    }

    disconnectedCallback(){
        unsubscribe(this.subscription);
        this.subscription=null;
    }

    meetingDetails(resp){

        if(resp){
            this.isMeetingDataFound = true;
            this.meetingData={
                roomName : resp.meetingInfo.roomName,
                roomCapacity : resp.meetingInfo.roomCapacity
            }
        }
        
    }
}