import { LightningElement ,wire} from 'lwc';
import {createRecord, getRecord} from 'lightning/uiRecordApi';

const fieldsArr =['Account.Name','Account.Phone','Account.Website'];

export default class AccountManagerLds extends LightningElement {

    accoutName;
    accountPhone;
    accountWebsite;
    accId;

    @wire(getRecord, {recordId: '$accId',fields: fieldsArr}) accountRecord;

    accountNameHandler(event){
        this.accoutName = event.target.value;
    }

    accountPhoneHandler(event){
        this.accountPhone = event.target.value;
    }

    accountWebsiteHandler(event){
        this.accountWebsite = event.target.value;
    }

    createAccount(){
        const fields = {'Name':this.accoutName,'Phone':this.accountPhone,'Website':this.accountWebsite};
        const recordInput = {apiName :'Account', fields};
        createRecord(recordInput).then(response =>{
            this.accId = response.id;
            console.log('Account is created '+ response.id);
        }).catch(error =>{
            console.error('error '+error.body.message);
        });
    }

}