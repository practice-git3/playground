import { LightningElement ,api,track} from 'lwc';

export default class PublicMethodChild extends LightningElement {

    @track value=['Red'];

    options=[
        {label:'Red Marker' ,value:'Red'},
        {label:'Blue Marker' ,value:'Blue'},
        {label:'Green Marker' ,value:'Green'},
        {label:'Yellow Marker' ,value:'Yellow'}
    ];

    @api
    selectCheckbox(checkboxValue){

        const selectedCheckbox = this.options.find(checkbox =>{
            return checkbox.value === checkboxValue;
            
        })
        console.log('checl',selectedCheckbox);
        if(selectedCheckbox){
            this.value = selectedCheckbox.value;
            return 'Successfully Checked';
        }
        
        return 'No Check Found';

    }
}