import { LightningElement } from 'lwc';

export default class MeetingRooms extends LightningElement {

    selectedRoomInfo;
    meetingRoomInfo =[
        {roomName:'A-01',roomCapacity:1},
        {roomName:'A-02',roomCapacity:3},
        {roomName:'A-03',roomCapacity:5},
        {roomName:'B-01',roomCapacity:2},
        {roomName:'B-02',roomCapacity:1},
        {roomName:'C-01',roomCapacity:1},
    ];

    titleClickHandler(event){

        this.selectedRoomInfo = event.detail.roomName;
    }

    constructor(){
        super();

        this.template.addEventListener('titleclick',this.titleClickHandler.bind(this));
    }
}