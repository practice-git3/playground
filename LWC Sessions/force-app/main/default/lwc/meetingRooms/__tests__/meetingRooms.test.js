import {createElement} from 'lwc';
import meetingRooms from 'c/meetingRooms';

describe('c-meetingRooms',() =>{

    it('count of meeting rooms',() =>{

        const meetingRoom = createElement('c-meeting-rooms',{is: meetingRooms});

        document.body.appendChild(meetingRoom);

        const meetingRoomComponents =  meetingRoom.shadowRoot.this.template.querySelectorAll('c-meeting-room');

        expect(meetingRoomComponents.length).toBe(7);
    });
});