import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
export default class NavigationExample extends NavigationMixin(LightningElement) {

    openSfdcFacts(){

        this[NavigationMixin.Navigate]({
            type : 'standard__webPage',
            attributes :{
                url : 'https://sfdcfacts.com'
            }
        });
    }
    openAccountHome(){

        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Account',
                actionName: 'home',
            },
        });
    }

    openCaseListView(){

        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Case',
                actionName: 'list',
            },
        });
    }

    openContactRecord(){

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: '0031y00000DnGBqAAN',
                objectApiName: 'Contact',
                actionName: 'view',
            },
        });

    }

    openLDS(){

        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'LDS'
            },
        });


    }
    
}