import { LightningElement } from 'lwc';

export default class PublicMethodParent extends LightningElement {

    value;

    colorChangeHandler(event){
        this.value = event.target.value;
    }

    checkboxSelectHandler(){

        const childComponent = this.template.querySelector('c-public-method-child');
        const result = childComponent.selectCheckbox(this.value);
        console.log('result '+result);
    }
}