import { LightningElement ,api,wire} from 'lwc';
import meetingInfo from'@salesforce/messageChannel/meetingInfo__c';
import {MessageContext,publish} from'lightning/messageService';

export default class MeetingRoom extends LightningElement {

    @api meetingRoomInfo;//{roomName:'A-01,roomCapacity:1}
    @api showRoomInfo = false;

    @wire(MessageContext)meetingContext;
    tileClickHandler(){
        const tileClicked = new CustomEvent('titleclick',{detail:this.meetingRoomInfo,bubbles:true});
        this.dispatchEvent(tileClicked);
        const msgPayload={
            meetingInfo :this.meetingRoomInfo
        };
        console.log("msg payload "+msgPayload);
        publish(this.meetingContext,meetingInfo,msgPayload);
    }
}