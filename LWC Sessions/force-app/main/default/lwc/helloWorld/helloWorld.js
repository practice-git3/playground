import { LightningElement } from 'lwc';

export default class HelloWorld extends LightningElement {
    greeting = 'World!';

    greetingChangeHandler(event){

        this.greeting = event.target.value;
    }
}