import { LightningElement,track, wire } from 'lwc';
import getAllObjects from '@salesforce/apex/ObjectMetaData.getAllObjects';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import objectFieldsData from '@salesforce/apex/ObjectMetaData.objectFieldsData';
export default class ObjectMetaData extends LightningElement {

    @track objectList =[];
    selectedValue;
    @track objFieldsData =[];
    initialized = false;
    fetchedFields = false;
    sortBy;
    sortDirection;
    columns = [
            { label: 'Field Label', fieldName: 'fieldLabel' },
            { label: 'Field Name', fieldName: 'fieldName',sortable: "true" },
            { label: 'Data Type', fieldName: 'fieldDataType',sortable: "true"}
        ];
    
    sortHandler(event){

        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;

        this.sortData(this.sortBy,this.sortDirection);

    }

    
    getObjectFieldsData(event){
        if(event.target.value){
            this.selectedValue = event.target.value;
        }
        
    }

    @wire(objectFieldsData,{objectName:'$selectedValue'})
    wiredFieldData( {error, data }){
        if(data){
            
            this.fetchedFields = true;
            this.objFieldsData = JSON.parse(data);
        }
        else if(error){
            console.error('error in fetching fields '+error.body.message);
            const toastEvent = new ShowToastEvent({
                title: 'ERROR',
                message: error.body.message,
                variant:'error'
            });
            this.dispatchEvent(toastEvent);
        }
    }    

    sortData(fieldName,direction){

        console.log('this.objectFieldsData '+ this.objFieldsData);
        let sortData = JSON.parse(JSON.stringify(this.objFieldsData));
        
        console.log('sortData '+sortData);
        let keyValue  = (a) =>{
            return a[fieldName];
        }

        let isReverse = direction === 'asc' ? 1: -1;

        sortData.sort((x,y) =>{
            x = keyValue(x) ? keyValue(x) :'';
            y = keyValue(y) ? keyValue(y) :'';

            return isReverse *((x > y) - (y > x));

        });
        this.objFieldsData = sortData;

    }

    connectedCallback(){

        this.getObjectData();
    }

    renderedCallback() {
        if (this.initialized) {
            return;
        }
        this.initialized = true;
        let listId = this.template.querySelector('datalist').id;
        this.template.querySelector('input').setAttribute('list', listId);
    }

    getObjectData(){

        getAllObjects().then(result =>{

            for(let key in result){

                if (result.hasOwnProperty(key)) { // Filtering the data in the loop
                    this.objectList.push({value:result[key], key:key});
                }   
            }
            console.log('resukt '+result);

        }).catch(error =>{

            console.error('error in fetching '+error.body.message);
            const toastEvent = new ShowToastEvent({
                title: 'ERROR',
                message: error.body.message,
                variant:'error'
            });
            this.dispatchEvent(toastEvent);
      
        });
    }
}