import { LightningElement } from 'lwc';

export default class ConditionalRendering extends LightningElement {

    displayDiv = false;
    cityList = ['Guntur','Vijayawada','Nellore','Hyderbad'];
    showDivHandler(event){
        this.displayDiv = event.target.checked;
    }
}